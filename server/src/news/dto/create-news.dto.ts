export class CreateNewsDto {
  newsId: string;
  author: string;
  title: string;
  url: string;
  createdAt: Date;
}
