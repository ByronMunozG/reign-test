import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { News, NewsDocument } from './schemas/news.schema';

@Injectable()
export class NewsRepository {
  constructor(@InjectModel(News.name) private newsModel: Model<NewsDocument>) {}

  async find() {
    return this.newsModel
      .find({ isActive: true })
      .sort({ createdAt: -1 })
      .exec();
  }

  async findByNewsId(newsId: string) {
    return this.newsModel.findOne({ newsId });
  }

  async create(news: News): Promise<News> {
    const newNews = new this.newsModel(news);
    return newNews.save();
  }

  async delete(newsId: string): Promise<News> {
    const news = await this.newsModel.findOne({ newsId });
    news.isActive = false;
    return await news.save();
  }
}
