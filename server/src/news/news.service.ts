import { Injectable } from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { NewsRepository } from './news.repository';
import { News } from './schemas/news.schema';

@Injectable()
export class NewsService {
  constructor(private readonly newsRepository: NewsRepository) {}

  async createNews(createNewsDto: CreateNewsDto): Promise<News> {
    return this.newsRepository.create({ ...createNewsDto, isActive: true });
  }

  async findAll(): Promise<News[]> {
    return this.newsRepository.find();
  }

  async findById(newsId: string): Promise<News> {
    return this.newsRepository.findByNewsId(newsId);
  }

  async deleteById(newsId: string): Promise<News> {
    return this.newsRepository.delete(newsId);
  }
}
