// mocks the newsService and create methods with expected values
// spy the methods
import { newsStub } from '../test/stubs/news.stub';

export const NewsService = jest.fn().mockReturnValue({
  createNews: jest.fn().mockResolvedValue(newsStub()),
  findAll: jest.fn().mockResolvedValue([newsStub()]),
  findById: jest.fn().mockResolvedValue(newsStub()),
  deleteById: jest.fn().mockResolvedValue(newsStub({ isActive: false })),
});
