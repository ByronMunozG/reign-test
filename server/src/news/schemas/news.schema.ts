import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewsDocument = News & Document;

@Schema({ versionKey: false })
export class News {
  @Prop()
  newsId: string;

  @Prop()
  author: string;

  @Prop()
  title: string;

  @Prop()
  url: string;

  @Prop()
  createdAt: Date;

  @Prop()
  isActive: boolean;
}

export const NewsSchema = SchemaFactory.createForClass(News);
