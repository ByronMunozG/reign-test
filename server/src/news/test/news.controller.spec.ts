import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from '../news.controller';
import { NewsService } from '../news.service';
import { News } from '../schemas/news.schema';
import { newsStub } from './stubs/news.stub';

jest.mock('../news.service');

describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [],
      controllers: [NewsController],
      providers: [NewsService],
    }).compile();

    newsController = moduleRef.get<NewsController>(NewsController);
    newsService = moduleRef.get<NewsService>(NewsService);
    jest.clearAllMocks();
  });

  describe('findAll', () => {
    describe('when findAll is called', () => {
      let newsList: News[];

      beforeEach(async () => {
        newsList = await newsController.findAll();
      });

      test('then it should call newsService', () => {
        expect(newsService.findAll).toHaveBeenCalled();
      });

      test('then it should return an array of news', () => {
        expect(newsList).toEqual([newsStub()]);
      });
    });
  });

  describe('remove', () => {
    describe('when remove is called', () => {
      let news: News;

      beforeEach(async () => {
        news = await newsController.remove(newsStub().newsId);
      });

      test('then it should call newsService', () => {
        expect(newsService.deleteById).toHaveBeenCalledWith(newsStub().newsId);
      });

      test('then it should return a non-active news', () => {
        expect(news).toEqual({ ...newsStub(), isActive: false });
      });
    });
  });
});
