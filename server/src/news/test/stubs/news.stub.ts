import { News } from 'src/news/schemas/news.schema';

export const newsStub = (data = {}): News => ({
  newsId: '123',
  author: 'Byron',
  createdAt: new Date('05/11/2021'),
  title: 'news title',
  url: 'https://newstest.com/exciting-news',
  isActive: true,
  ...data,
});
