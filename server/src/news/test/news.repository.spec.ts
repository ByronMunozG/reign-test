import { getModelToken } from '@nestjs/mongoose';
import { Test } from '@nestjs/testing';
import { Model } from 'mongoose';
import { NewsRepository } from '../news.repository';
import { News, NewsDocument } from '../schemas/news.schema';
import { newsStub } from './stubs/news.stub';
import {
  NewsModelFindOperations,
  NewsModelCreateOperations,
} from './support/news.model';

describe('NewsRepository', () => {
  let newsRepository: NewsRepository;

  describe('find operations', () => {
    let newsModel: Model<NewsDocument>;

    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [
          NewsRepository,
          {
            provide: getModelToken(News.name),
            useClass: NewsModelFindOperations,
          },
        ],
      }).compile();

      newsRepository = moduleRef.get<NewsRepository>(NewsRepository);
      newsModel = moduleRef.get<Model<NewsDocument>>(getModelToken(News.name));

      jest.clearAllMocks();
    });

    describe('find', () => {
      describe('when find is called', () => {
        let news: News[];

        beforeEach(async () => {
          news = await newsRepository.find();
        });

        test('then it should call the newsModel', () => {
          expect(newsModel.find).toHaveBeenCalled();
        });

        test('then it should return an array of news', () => {
          expect(news).toEqual([newsStub()]);
        });
      });
    });

    describe('findByNewsId', () => {
      describe('when findByNewsId is called', () => {
        let news: News;

        beforeEach(async () => {
          news = await newsRepository.findByNewsId(newsStub().newsId);
        });

        test('then it should call the newsModel', () => {
          expect(newsModel.findOne).toHaveBeenCalled();
        });

        test('then it should return a news', () => {
          expect(news).toEqual(newsStub());
        });
      });
    });
  });

  describe('create operations', () => {
    beforeEach(async () => {
      const moduleRef = await Test.createTestingModule({
        providers: [
          NewsRepository,
          {
            provide: getModelToken(News.name),
            useValue: NewsModelCreateOperations,
          },
        ],
      }).compile();

      newsRepository = moduleRef.get<NewsRepository>(NewsRepository);

      jest.clearAllMocks();
    });
    describe('create', () => {
      describe('when create is called', () => {
        let news: News;
        let saveSpy: jest.SpyInstance;
        let constructorSpu: jest.SpyInstance;

        beforeEach(async () => {
          saveSpy = jest.spyOn(NewsModelCreateOperations.prototype, 'save');
          constructorSpu = jest.spyOn(
            NewsModelCreateOperations.prototype,
            'constructorSpy',
          );
          news = await newsRepository.create(newsStub());
        });

        test('then it should call the newsModel', () => {
          expect(saveSpy).toHaveBeenCalled();
          expect(constructorSpu).toHaveBeenCalledWith(newsStub());
        });

        test('then it should return a news', () => {
          expect(news).toEqual(newsStub());
        });
      });
    });
  });
});
