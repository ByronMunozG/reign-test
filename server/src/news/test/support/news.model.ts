import { News } from 'src/news/schemas/news.schema';
import { newsStub } from '../stubs/news.stub';

export const NewsModelFindOperations = jest.fn().mockReturnValue({
  find: jest.fn().mockImplementation(
    jest.fn().mockReturnValue({
      sort: jest.fn().mockReturnValue({
        exec: jest.fn().mockReturnValue([newsStub()]),
      }),
    }),
  ),
  findOne: jest.fn().mockImplementation(jest.fn().mockReturnValue(newsStub())),
});

export class NewsModelCreateOperations {
  protected newsStub: News = newsStub();

  /*eslint-disable */
  constructor(createEntityData: News) {
    this.constructorSpy(createEntityData);
  }
  constructorSpy(_createEntityData: News): void {}
  /*eslint-enable */

  async save(): Promise<News> {
    return this.newsStub;
  }
}
