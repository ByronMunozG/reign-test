import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { NewsCollectorModule } from 'src/news-collector/news-collector.module';
import { NewsModule } from 'src/news/news.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    MongooseModule.forRootAsync({
      useFactory: () => {
        const host = process.env.DB_HOST;
        const user = process.env.MONGO_USER;
        const pass = process.env.MONGO_PASS;
        return {
          uri: `mongodb://${user}:${pass}@${host}`,
          dbName: 'nest',
        };
      },
    }),
    ScheduleModule.forRoot(),
    NewsCollectorModule,
    NewsModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
