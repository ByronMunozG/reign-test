import { HttpService } from '@nestjs/axios';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { lastValueFrom } from 'rxjs';
import { NewsService } from 'src/news/news.service';

@Injectable()
export class NewsCollectorService implements OnModuleInit {
  private newsUrl = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

  constructor(
    private httpService: HttpService,
    private readonly newsService: NewsService,
  ) {}

  onModuleInit() {
    // Populate on startup..
    this.collectNews();
  }

  @Cron(CronExpression.EVERY_HOUR)
  async collectNews() {
    const response = await this.getNews();
    for (const news of response.data.hits) {
      if (!news.story_title && !news.title) continue;
      if (!news.url && !news.story_url) continue;

      const newsExist = await this.newsService.findById(news.objectID);
      if (newsExist) continue;

      await this.newsService.createNews({
        newsId: news.objectID,
        title: news.story_title ?? news.title,
        author: news.author,
        createdAt: new Date(news.created_at),
        url: news.url ?? news.story_url,
      });
    }
  }

  async getNews() {
    return await lastValueFrom(this.httpService.get(this.newsUrl));
  }
}
