import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { NewsModule } from 'src/news/news.module';
import { NewsCollectorService } from './news-collector.service';

@Module({
  imports: [HttpModule, NewsModule],
  controllers: [],
  providers: [NewsCollectorService],
})
export class NewsCollectorModule {}
