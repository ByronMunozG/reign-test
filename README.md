<h1 align="center">
    Reign Test
</h1>

<br/>

## Description
Technical test for fullstack developer position.

<br/>

## Before running the app
Make a empty folder called 'mongodb_data' on the root folder

<br/>

## Running the app

```bash
$ docker-compose up --build
```

<br/>

## Test

```bash
# first enter the server folder
$ cd server

# test coverage
$ npm run test:cov-report

# test linter
$ npm run lint
```

