const Header = () => {
  return (
    <header className="pl-12 py-14 bg-headerBg">
      <h1 className="text-white font-bold text-8xl">HN Feed</h1>
      <h2 className="text-white font-semibold text-2xl mt-2">
        {"We <3 hacker news!"}
      </h2>
    </header>
  );
};

export default Header;
