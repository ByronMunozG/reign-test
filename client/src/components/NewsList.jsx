import useFetch from "../hooks/useFetch";
import NewsItem from "./NewsItem";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import Loading from "./Loading";
import ErrorMessage from "./Error";

const NewsList = () => {
  const NEWS_URL = "http://localhost:8000/news";
  const { data, error, loading, setData } = useFetch(NEWS_URL);

  if (error) {
    return <ErrorMessage />;
  }

  const deleteItem = async (newsId) => {
    await axios.delete(`${NEWS_URL}/${newsId}`);
    setData((currentData) =>
      currentData.filter((news) => news.newsId !== newsId)
    );
    toast.success("Deleted!");
  };

  return loading ? (
    <Loading />
  ) : (
    <div>
      <ToastContainer position="top-right" />
      <ul className="my-5 mx-14">
        {data.map((news) => (
          <NewsItem key={news.newsId} news={news} handleDelete={deleteItem} />
        ))}
      </ul>
    </div>
  );
};

export default NewsList;
