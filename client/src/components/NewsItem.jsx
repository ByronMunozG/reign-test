import { isToday, isYesterday, format } from "date-fns";
import { ReactComponent as TrashIcon } from "../assets/trash-alt-solid.svg";

const NewsItem = ({ news, handleDelete }) => {
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    if (isToday(date)) {
      return format(date, "hh:mm aaa");
    } else if (isYesterday(date)) {
      return "Yesterday";
    } else {
      return format(date, "MMM d");
    }
  };

  const handleClick = (url) => {
    window.open(url, "_blank");
  };

  return (
    <li className="flex items-center justify-between px-5 py-5 font-semibold border-b cursor-pointer bg-fff border-ccc text-13 hover:bg-newsHover">
      <div
        className="flex justify-between flex-grow"
        onClick={() => handleClick(news.url)}
      >
        <div className="flex">
          <h3 className="text-newsTitle">{news.title}.</h3>
          <span className="ml-3 text-newsAuthor">- {news.author} -</span>
        </div>
        <span>{formatDate(news.createdAt)}</span>
      </div>
      <TrashIcon
        height="13pt"
        className="ml-20 cursor-pointer"
        onClick={() => handleDelete(news.newsId)}
      />
    </li>
  );
};
export default NewsItem;
