const Loading = () => (
  <div className="flex justify-center mt-14">
    <h3 className="text-4xl font-bold">Loading...</h3>
  </div>
);
export default Loading;
