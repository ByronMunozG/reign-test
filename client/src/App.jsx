import Header from "./components/Header";
import NewsList from "./components/NewsList";

const App = () => {
  return (
    <section className="">
      <Header />
      <NewsList />
    </section>
  );
};

export default App;
