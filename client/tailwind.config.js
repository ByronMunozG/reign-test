module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      body: ["Quicksand", "sans-serif"],
    },
    extend: {
      colors: {
        headerBg: "#333333",
        fff: "#fff",
        ccc: "#ccc",
        newsTitle: "#333",
        newsAuthor: "#999",
        newsHover: "#fafafa",
      },
      fontSize: {
        13: "13pt",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
